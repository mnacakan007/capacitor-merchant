import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  data: any;
  response: any;
  constructor(
      private barcodeScanner: BarcodeScanner,
      private http: HttpClient,
  ) {
  }

  scan() {
    this.data = null;
    this.response = null;
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.data = barcodeData;
      this.http.get('https://jsonplaceholder.typicode.com/todos/1').subscribe((res: any) => {
        this.response = res;
      });
    }).catch(err => {
      console.log('Error', err);
    });
  }

}
